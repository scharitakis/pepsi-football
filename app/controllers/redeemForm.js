/*Ti.App.addEventListener("sliderToggled", function(e) {
if (e.hasSlided) {
//	$.table.touchEnabled = false;
}
else {
//	$.table.touchEnabled = true;
}
});*/

// This will be triggered when

var RedeemFormData={};


function toggleErrorMsg(msg){
	if(msg!="")
	{
		//Show error
		$.errorMsgView.setVisible(true);
		$.errorMsgView.setHeight(Titanium.UI.SIZE);
		$.errorMsgText.setText(msg);
	}else{
		//Hide error
		$.errorMsgView.setVisible(false);
		$.errorMsgView.setHeight("0dp");
		$.errorMsgText.setText("");
	}
};

Ti.App.addEventListener("view:redeemForm", function(e) {
	//Ti.API.info("fired event view:redeemForm ------Captured");
	RedeemFormData=e;
	toggleErrorMsg("");
});



function deliverySubmit(){
	toggleErrorMsg("");
	var formValid = true;
	var errorMsg ="";
	var nameField = $.nameField.getValue();
	var surnameField = $.surnameField.getValue();
	var mobileNField = $.mobileNField.getValue();
	var addressField = $.addressField.getValue();
	var streetNField = $.streetNField.getValue();
	var cityField = $.cityField.getValue();
	var postCodeField = $.postCodeField.getValue();
	//TODO: need to add checks here 
	if(nameField=="")
	{
		errorMsg ="Ungültige Eingabe: NAME"; //"Invalid input: Name";
		formValid=false;
	}
	
	if(surnameField=="")
	{
		if(formValid){
			errorMsg ="Ungültige Eingabe: NACHNAME"; //"Invalid input: Surname";
			formValid=false;
		}
	}
	
	if(addressField=="")
	{
		if(formValid){
			errorMsg="Ungültige Eingabe: STRAßE"; //"Invalid input: Street";
			formValid=false;
		}
	}
	
	if(streetNField=="")
	{
		if(formValid){
			errorMsg="Ungültige Eingabe: NUMMER"; //"Invalid input: Street Number";
			formValid=false;
		}
	}
	
	if(cityField=="")
	{
		if(formValid){
			errorMsg="Ungültige Eingabe: STADT"; //"Invalid input: City";
			formValid=false;
		}
	}
	
	if(postCodeField=="")
	{
		if(formValid){
			errorMsg="Ungültige Eingabe:POSTLEIZAHL"; //"Invalid input: PostCode";
			formValid=false;
		}
	}
		
    var convertedMsisdn =""; 
        
    if(mobileNField!="")
	{
		var expression1 = new RegExp("^015\\d{9}$"); //015+9 digits
        var expression2 = new RegExp("^016\\d{8,9}$"); //016+8-9 digits
        var expression3 = new RegExp("^017\\d{8,9}$"); //017+8-9 digits

        var expression4 = new RegExp("^[+]4915\\d{9}$"); //+4915+9 digits
        var expression5 = new RegExp("^[+]4916\\d{8,9}$"); //+4916+8-9 digits
        var expression6 = new RegExp("^[+]4917\\d{8,9}$"); //+4917+8-9 digits

        var expression7 = new RegExp("^004915\\d{9}$"); //004915+9 digits
        var expression8 = new RegExp("^004916\\d{8,9}$"); //004916+8-9 digits
        var expression9 = new RegExp("^004917\\d{8,9}$"); //004917+8-9 digits
        
        var overallexpression = new RegExp("^00491(5\\d{9}|[67]\\d{8,9})$");
			
		if(expression1.test(mobileNField)){
			convertedMsisdn = "004915" + mobileNField.substring(3);
		}else if(expression2.test(mobileNField)){
			 convertedMsisdn = "004916" + mobileNField.substring(3);
		}
		else if(expression3.test(mobileNField)){
			 convertedMsisdn = "004917" + mobileNField.substring(3);
		}
		 else if(expression4.test(mobileNField)){
		 	 convertedMsisdn = "004915" + mobileNField.substring(5);
		 }
		 else if(expression5.test(mobileNField)){
		 	 convertedMsisdn = "004916" + mobileNField.substring(5);
		 }
		 else if(expression6.test(mobileNField)){
		 	  convertedMsisdn = "004917" + mobileNField.substring(5);
		 }
		 else if(expression7.test(mobileNField)){
		 	 convertedMsisdn = "004915" + mobileNField.substring(6);
		 } 
		 else if(expression8.test(mobileNField)){
		 	 convertedMsisdn = "004916" + mobileNField.substring(6);
		 } 
		 else if(expression9.test(mobileNField)){
			  convertedMsisdn = "004917" + mobileNField.substring(6);
		}else{
			convertedMsisdn = mobileNField;
		}
	}
	
	if(mobileNField=="")
	{
		if(formValid){
			errorMsg ="Bitte gebe deine Handynummer ein.";
			formValid=false;
		}
	}
	else if(!overallexpression.test(convertedMsisdn))
	{
			errorMsg ="Handynummer ist ungültig. Beispiel: 0049xxxxxxxxx";
			formValid=false;
	}
	
	if(formValid){
		$.mobileNField.setValue(convertedMsisdn);
		
		$.nameField.blur();
		$.surnameField.blur();
		$.mobileNField.blur();
		$.addressField.blur();
		$.streetNField.blur();
		$.cityField.blur();
		$.postCodeField.blur();
		
		RedeemFormData.nameField = nameField;
		RedeemFormData.surnameField = surnameField;
		RedeemFormData.mobileNField = convertedMsisdn;
		RedeemFormData.addressField = addressField;
		RedeemFormData.streetNField = streetNField;
		RedeemFormData.cityField = cityField;
		RedeemFormData.postCodeField = postCodeField;
		
		Ti.App.fireEvent("changeView", 
				{row : {name:"Überprüfung",viewsrc:"reviewRedeemForm",props:RedeemFormData,scroll:false}}
	    );
	 }else{
	 	toggleErrorMsg(errorMsg);
	 }
}



