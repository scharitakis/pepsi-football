var data ={};


var indicator=false;

function toggleIndicator(){
	if(indicator)
	{
		$.disableViewBgLoadingIndicator.setVisible(false);
		$.activityIndicator.hide();
		indicator=false;
	}else{
		$.disableViewBgLoadingIndicator.setVisible(true);
		if (Ti.Platform.osname === 'iphone'){		
			$.activityIndicator.setStyle(Titanium.UI.iPhone.ActivityIndicatorStyle.BIG);
		}else{
			$.activityIndicator.setStyle(Titanium.UI.ActivityIndicatorStyle.BIG);
		}
		$.activityIndicator.show();
		indicator=true;
	}
};



// This will be triggered when
Ti.App.addEventListener("view:winnerView", function(e) {
	data=e;	
});

function doClose(){
	Ti.App.fireEvent("changeView", 
			{row : {name:"Home",viewsrc:"home",props:null,scroll:false}}
    );
}

function sendPinWinnerSuccessClb(){
	//var data = JSON.parse(this.responseText);
	toggleIndicator();
	if(data.result=="SUCCESS")
	{
		//We need to show to the user the form to put his/her pin
		Ti.App.fireEvent("changeView", 
			{row : {name:"Pin",viewsrc:"pinDialog",props:{"user":true,"data":data},scroll:false}}
    	);
	}
}

function sendPinWinnerErrorClb(){
	//alert(e);
	toggleIndicator();
	toggleErrorMsg("Es gab ein Problem. Bitte wieder versuchen");
};


function doRedeemForm(e){
	if(!Ti.App.Properties.hasProperty('user')){
			if(Alloy.Globals.online){
				toggleIndicator();
				var msisdn = Alloy.Globals.userMsisdn;
				var url = Alloy.Globals.serverUrl+"sendConfirmationCode?msisdn="+msisdn+"&confirmationChannel=MOBILE_APP";
				Alloy.Globals.doGET(url,sendPinWinnerSuccessClb,sendPinWinnerErrorClb);
			};
	}else{
		Ti.App.fireEvent("changeView", 
			{row : {name:"Lieferung",viewsrc:"redeemForm",props:data,scroll:false}}
    	);
   }
}
