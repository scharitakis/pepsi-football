

var indicator=false;

function toggleIndicator(){
	if(indicator)
	{
		$.disableViewBgLoadingIndicator.setVisible(false);
		$.activityIndicator.hide();
		indicator=false;
	}else{
		$.disableViewBgLoadingIndicator.setVisible(true);
		if (Ti.Platform.osname === 'iphone'){		
			$.activityIndicator.setStyle(Titanium.UI.iPhone.ActivityIndicatorStyle.BIG);
		}else{
			$.activityIndicator.setStyle(Titanium.UI.ActivityIndicatorStyle.BIG);
		}
		$.activityIndicator.show();
		indicator=true;
	}
};


function toggleErrorMsg(msg){
	if(msg!="")
	{
		//Show error
		$.errorMsgView.setVisible(true);
		$.errorMsgView.setHeight(Titanium.UI.SIZE);
		$.errorMsgText.setText(msg);
	}else{
		//Hide error
		$.errorMsgView.setVisible(false);
		$.errorMsgView.setHeight("0dp");
		$.errorMsgText.setText("");
	}
};


$.nameL.setText("");
$.surnameL.setText("");
$.streetL.setText("");
$.streetNL.setText("");
$.cityL.setText("");
$.postCL.setText("");
$.mobL.setText("");

var reviewRedeemFormData ={}; 


var indicator=false;

function toggleIndicator(){
	if(indicator)
	{
		$.disableViewBgLoadingIndicator.setVisible(false);
		$.activityIndicator.hide();
		indicator=false;
	}else{
		$.disableViewBgLoadingIndicator.setVisible(true);
		if (Ti.Platform.osname === 'iphone'){		
			$.activityIndicator.setStyle(Titanium.UI.iPhone.ActivityIndicatorStyle.BIG);
		}else{
			$.activityIndicator.setStyle(Titanium.UI.ActivityIndicatorStyle.BIG);
		}
		$.activityIndicator.show();
		indicator=true;
	}
}


function doChange(e){
	Ti.App.fireEvent("changeView", 
			{row : {name:"Überprüfung",viewsrc:"redeemForm",props:reviewRedeemFormData,scroll:false}}
    );
};

function RedeemFormSuccessClb(e){
	toggleIndicator();
	var data = JSON.parse(this.responseText);
	if(data.result=="SUCCESS"){
		Ti.App.fireEvent("changeView", 
			{row : {name:"Erfolg",viewsrc:"redeemSuccess",props:reviewRedeemFormData,scroll:false}}
    	);
	}else{
		toggleErrorMsg("Es gab ein Problem. Bitte wieder versuchen");
		alert("failed"+ JSON.stringify(data));
	}
};

function RedeemFormErrorClb(e){
	toggleIndicator();
	toggleErrorMsg("Es gab ein Problem. Bitte wieder versuchen");
	
	//alert("error"+e);
};


function sendDetails(e)
{
	if(Alloy.Globals.online){
		toggleIndicator();
		var confirmationCode = Ti.App.Properties.getObject("user").pin;
		var url = Alloy.Globals.serverUrl + "submitDeliveryData?memberId="+reviewRedeemFormData.memberId+"&confirmationCode="+confirmationCode+"&conversationId="+reviewRedeemFormData.conversationId+"&name="+reviewRedeemFormData.nameField+"&surname="+reviewRedeemFormData.surnameField+"&street="+reviewRedeemFormData.addressField+"&streetNo="+reviewRedeemFormData.streetNField+"&postalCode="+reviewRedeemFormData.postCodeField+"&city="+reviewRedeemFormData.cityField+"&mobile="+reviewRedeemFormData.mobileNField+"&channel=MOBILE_APP";
		Alloy.Globals.doGET(url,RedeemFormSuccessClb,RedeemFormErrorClb);	
	}
};



Ti.App.addEventListener("view:reviewRedeemForm", function(e) {
	//Ti.API.info("fired event view:reviewRedeemForm ------Captured");
	//alert(e);
	reviewRedeemFormData = e;
	toggleErrorMsg("");
	$.nameL.setText(e.nameField);
	$.surnameL.setText(e.surnameField);
	$.streetL.setText(e.addressField);
	$.streetNL.setText(e.streetNField);
	$.cityL.setText(e.cityField);
	$.postCL.setText(e.postCodeField);
	$.mobL.setText(e.mobileNField);

});