/*Ti.App.addEventListener("sliderToggled", function(e) {
if (e.hasSlided) {
//	$.table.touchEnabled = false;
}
else {
//	$.table.touchEnabled = true;
}
});*/

var indicator=false;

function toggleIndicator(){
	if(indicator)
	{
		$.disableViewBgLoadingIndicator.setVisible(false);
		$.activityIndicator.hide();
		indicator=false;
	}else{
		$.disableViewBgLoadingIndicator.setVisible(true);
		if (Ti.Platform.osname === 'iphone'){		
			$.activityIndicator.setStyle(Titanium.UI.iPhone.ActivityIndicatorStyle.BIG);
		}else{
			$.activityIndicator.setStyle(Titanium.UI.ActivityIndicatorStyle.BIG);
		}
		$.activityIndicator.show();
		indicator=true;
	}
};

function toggleErrorMsg(msg){
	if(msg!="")
	{
		//Show error
		$.errorMsgView.setVisible(true);
		$.errorMsgView.setHeight(Titanium.UI.SIZE);
		$.errorMsgText.setText(msg);
	}else{
		//Hide error
		$.errorMsgView.setVisible(false);
		$.errorMsgView.setHeight("0dp");
		$.errorMsgText.setText("");
	}
};


function openPopupMsgView(e){
	//$.popUpView.setVisible(true);
	
}

function closePopupMsgView(e){
	//$.popUpView.setVisible(false);
	//$.loadingIndicator.setVisible(false);
	
}


// The first the view is visible check if we have a user
if(Ti.App.Properties.hasProperty('user')){
		$.msisdnField.setValue(Ti.App.Properties.getObject('user')['msisdn']);
		$.msisdnField.enabled=false;
}else{
	$.msisdnField.enabled=true;
}

// This will be triggered when
Ti.App.addEventListener("view:coupon", function(e) {
	toggleErrorMsg("");
	$.couponField.setValue("");
	if(Ti.App.Properties.hasProperty('user')){
		$.msisdnField.setValue(Ti.App.Properties.getObject('user')['msisdn']);
		$.msisdnField.enabled=false;
	}else{
		$.msisdnField.enabled=true;
		$.msisdnField.setValue("");
	}
});

function redeemSuccessClb(e){
	var data = JSON.parse(this.responseText);
	//alert(data);
	toggleIndicator();
	//alert(JSON.stringify(data));
	if(data.result=="SUCCESS")
	{
		if(data.isBigPrizeWinning){
				Ti.App.fireEvent("changeView", 
					{row : {name:"Gewinner",viewsrc:"winnerView",props:data,scroll:false}}
	    		);
	    }
	    else if(data.couponStatus == 400 || data.couponStatus == 200 || data.couponStatus == 700){
	    	toggleErrorMsg(data.replyText);
	    }else{
    		Ti.App.fireEvent("changeView", 
				{row : {name:"Kein Sieg",viewsrc:"nowinnerView",props:data,scroll:false}}
    		);
    	}
	}else{
		toggleErrorMsg(data.replyText);
	}
		
};

function redeemErrorClb(e){
	toggleIndicator();
	//alert("error"+JSON.stringify(e));
	toggleErrorMsg("Es gab ein Problem bei der einlösung. Bitte wieder versuchen.");
	
};


function sendPinSuccessClb(){
	var data = JSON.parse(this.responseText);
	toggleIndicator();
	if(data.result=="SUCCESS")
	{
		//We need to show to the user the form to put his/her pin
		Ti.App.fireEvent("changeView", 
			{row : {name:"Pin",viewsrc:"pinDialog",props:{user:false},scroll:false}}
    	);
	}
};


function sendPinErrorClb(){
	//alert(e);
	toggleErrorMsg("Es gab ein Problem. Bitte wieder versuchen");
};

function msisdnCheckSuccessClb(e){
	var data = JSON.parse(this.responseText);
	var msisdn = $.msisdnField.getValue();
	//alert(data);
	if(!data.msisdnRegistered) // User not registered
	{
		// send the pin to user
		if(Alloy.Globals.online){
			var url = Alloy.Globals.serverUrl+"sendConfirmationCode?msisdn="+msisdn+"&confirmationChannel=MOBILE_APP";
			Alloy.Globals.userMsisdn = msisdn;
			Alloy.Globals.doGET(url,sendPinSuccessClb,sendPinErrorClb);
		};
	}else{
		//if user is registered then send the code
			Alloy.Globals.setupUI(true); //Change the menu
			Ti.App.Properties.setString('userMenu', msisdn); //User is registered so you can change the menu
			if(Alloy.Globals.online){
				var msisdn = $.msisdnField.getValue();//79100000101
				var coupon = $.couponField.getValue();
				Alloy.Globals.userMsisdn = msisdn;
				Alloy.Globals.PushToken = (Ti.App.Properties.hasProperty('PushToken'))?Ti.App.Properties.getString('PushToken'):Alloy.Globals.PushToken;
				var url =Alloy.Globals.serverUrl+"redeemCoupon?coupon="+coupon+"&msisdn="+msisdn+"&channel=MOBILE_APP&userAgent=MOBILE_APP&locale=null&pushToken="+Alloy.Globals.PushToken+"&device="+Alloy.Globals.isiOS; 
				Alloy.Globals.doGET(url,redeemSuccessClb,redeemErrorClb);
			};
		
	}
};

function msisdnCheckErrorClb(e){
	toggleIndicator();
	toggleErrorMsg("Server-Fehler");
	//alert("msisdnCheckErrorClb error"+ JSON.stringify(e));
};


function redeemCoupon(){
	//First Check if the MSISDN is Valid
	$.msisdnField.blur();
	$.couponField.blur();
	toggleErrorMsg("");
	if(Alloy.Globals.online){
		
		var expression1 = new RegExp("^015\\d{9}$"); //015+9 digits
        var expression2 = new RegExp("^016\\d{8,9}$"); //016+8-9 digits
        var expression3 = new RegExp("^017\\d{8,9}$"); //017+8-9 digits

        var expression4 = new RegExp("^[+]4915\\d{9}$"); //+4915+9 digits
        var expression5 = new RegExp("^[+]4916\\d{8,9}$"); //+4916+8-9 digits
        var expression6 = new RegExp("^[+]4917\\d{8,9}$"); //+4917+8-9 digits

        var expression7 = new RegExp("^004915\\d{9}$"); //004915+9 digits
        var expression8 = new RegExp("^004916\\d{8,9}$"); //004916+8-9 digits
        var expression9 = new RegExp("^004917\\d{8,9}$"); //004917+8-9 digits
        
        var overallexpression = new RegExp("^00491(5\\d{9}|[67]\\d{8,9})$");
		
		var msisdn = $.msisdnField.getValue();//79100000101
		var coupon = $.couponField.getValue();
		var formValid=true;
		var errorMsg ="";
		var convertedMsisdn ="";
		if(msisdn!="")
		{
			
			if(expression1.test(msisdn)){
				convertedMsisdn = "004915" + msisdn.substring(3);
			}else if(expression2.test(msisdn)){
				 convertedMsisdn = "004916" + msisdn.substring(3);
			}
			else if(expression3.test(msisdn)){
				 convertedMsisdn = "004917" + msisdn.substring(3);
			}
			 else if(expression4.test(msisdn)){
			 	 convertedMsisdn = "004915" + msisdn.substring(5);
			 }
			 else if(expression5.test(msisdn)){
			 	 convertedMsisdn = "004916" + msisdn.substring(5);
			 }
			 else if(expression6.test(msisdn)){
			 	  convertedMsisdn = "004917" + msisdn.substring(5);
			 }
			 else if(expression7.test(msisdn)){
			 	 convertedMsisdn = "004915" + msisdn.substring(6);
			 } 
			 else if(expression8.test(msisdn)){
			 	 convertedMsisdn = "004916" + msisdn.substring(6);
			 } 
			 else if(expression9.test(msisdn)){
				  convertedMsisdn = "004917" + msisdn.substring(6);
			}else{
				convertedMsisdn = msisdn;
			}
		}
		
		if(msisdn=="")
		{
			errorMsg ="Bitte gebe deine Handynummer ein.";
			formValid=false;
		}
		else if(!overallexpression.test(convertedMsisdn))
		{
			errorMsg ="Handynummer ist ungültig. Beispiel: 0049xxxxxxxxx";
			formValid=false;
		}else if(coupon==""){
			errorMsg ="Deckelcode bitte eingeben.";
			formValid=false;
		}
		
		
		if(formValid){
			$.msisdnField.setValue(convertedMsisdn);
			toggleIndicator();
			var url = Alloy.Globals.serverUrl + "isMsisdnRegistered?msisdn="+convertedMsisdn;
			////Ti.API.info(url);
			Alloy.Globals.doGET(url,msisdnCheckSuccessClb,msisdnCheckErrorClb);
		}else{
			toggleErrorMsg(errorMsg);
		}
	}
}


function showLoginPage(){
	Ti.App.fireEvent("changeView", 
	{row : {name:"Login",viewsrc:"login",props:null,scroll:true}}
	);
}
