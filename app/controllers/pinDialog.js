var indicator=false;

function toggleIndicator(){
	if(indicator)
	{
		$.disableViewBgLoadingIndicator.setVisible(false);
		$.activityIndicator.hide();
		indicator=false;
	}else{
		$.disableViewBgLoadingIndicator.setVisible(true);
		if (Ti.Platform.osname === 'iphone'){		
			$.activityIndicator.setStyle(Titanium.UI.iPhone.ActivityIndicatorStyle.BIG);
		}else{
			$.activityIndicator.setStyle(Titanium.UI.ActivityIndicatorStyle.BIG);
		}
		$.activityIndicator.show();
		indicator=true;
	}
}

function toggleErrorMsg(msg){
	if(msg!="")
	{
		//Show error
		$.errorMsgView.setVisible(true);
		$.errorMsgView.setHeight(Titanium.UI.SIZE);
		$.errorMsgText.setText(msg);
	}else{
		//Hide error
		$.errorMsgView.setVisible(false);
		$.errorMsgView.setHeight("0dp");
		$.errorMsgText.setText("");
	}
};

var WinnerData={};
var goToForm = false;
Ti.App.addEventListener("view:pinDialog",function(e){
	//alert(e);
	WinnerData={};
	goToForm=false;
	//Ti.API.info("fired event view:pinDialog ------Captured");
	toggleErrorMsg("");
	if(e.user){
		WinnerData = e.data;
		goToForm=true;
	}
});

function successPinClb(e){
	var data = JSON.parse(this.responseText);
	//alert(data);
	toggleIndicator();
	if(data.result=="SUCCESS"){
		// need to save the user
		var pinVal = $.pinField.getValue();
		Ti.App.Properties.setObject('user',{msisdn:Alloy.Globals.userMsisdn,pin:pinVal});
		Alloy.Globals.setupUI(true);//change the menu 
		if(!goToForm){
			var goBackView = Alloy.Globals.previousView;
			goBackView.row.scroll=false;
			Ti.App.fireEvent("changeView", 
				goBackView
			);
		}else{
			Ti.App.fireEvent("changeView", 
				{row : {name:"Lieferung",viewsrc:"redeemForm",props:WinnerData,scroll:false}}
    		);			
		}
	}else{
		if(data.failReason=="INVALID_CONFIRMATION_CODE")
		{
			toggleErrorMsg("Falschen Pin. Bitte wieder versuchen.");	
		}		
	}
}


function errorPinClb(){
	//alert(e);
	toggleIndicator();
	toggleErrorMsg("Server-Fehler");	
}

function submitPin(e){
	$.pinField.blur();
	toggleErrorMsg("");
	if(Alloy.Globals.online){
		toggleIndicator();
		var msisdn = Alloy.Globals.userMsisdn;
		var pinVal = $.pinField.getValue();
		var url = Alloy.Globals.serverUrl + "confirmMsisdn?msisdn="+msisdn+"&confirmationChannel=MOBILE_APP&confirmationCode="+pinVal+"&registrationSite=MOBILE_APP&userAgent=MOBILE_APP&pushToken="+Alloy.Globals.PushToken+"&device="+Alloy.Globals.isiOS;
		Alloy.Globals.doGET(url,successPinClb,errorPinClb);
	}
}
