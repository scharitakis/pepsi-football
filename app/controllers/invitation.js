
var allowInvitation = false;
var smsMsgBody = "";
var emailMsgBody = "";

function viralUniqueUrlSuccessClb (){
	var data = JSON.parse(this.responseText);
	if(data.result=="SUCCESS")
	{
		allowInvitation = true;
		//We need to show to the user the form to put his/her pin
		var urls = data.viralUniqueUrls;
		var email_url = urls.VIRAL_MOBILE_APP_EMAIL;
		var sms_url = urls.VIRAL_MOBILE_APP_SMS;
		smsMsgBody = 'Hi! \n'+
			'Bei Pepsi kannst du 1 von 10.000 XBOX ONE gewinnen! \n'+ 
			'Sei auch dabei und du kannst gewinnen! \n'+
			'Hier registrieren: '+sms_url;
			
		emailMsgBody =	'Hi! \n'+
			'Bei Pepsi kannst du 1 von 10.000 XBOX ONE und viele weitere phantastische Preise gewinnen! \n'+
			'Ich nehme bereits teil! \n'+
			'Mach auch mit und du kannst gewinnen! \n \n'+
			'Geh auf '+email_url+' und sei bei der nächsten Ziehung dabei!';
			
	    $.copyLink.setText(email_url);
				
	}else{
		//error
	}
};

function viralUniqueUrlErrorClb (){
	//error
};

Ti.App.addEventListener("view:invitation", function(e) {	
	// get the tiny urls
	var msisdn ="";
	if(Alloy.Globals.userMsisdn==""){
		msisdn = Ti.App.Properties.getString('userMenu');
	}else{
		msisdn = Alloy.Globals.userMsisdn;
	}
	
	var url =Alloy.Globals.serverUrl+"getViralUniqueUrls?msisdn="+msisdn+"&channel=MOBILE_APP";
	Alloy.Globals.doGET(url,viralUniqueUrlSuccessClb,viralUniqueUrlErrorClb);
});





function doClose(){
	Ti.App.fireEvent("changeView", 
			{row : {name:"Home",viewsrc:"home",props:null,scroll:false}}
    );
}

function openPopup(){
	var w = Alloy.createController("invitationHowTo").getView();
	
	w.setTop(600);
	var a = Titanium.UI.createAnimation();
	if(Ti.Platform.version.indexOf("6")!=0)// Check if ios version is 6 or not
	{
		a.top = 60;
	}else{
		a.top = 40;
	}
	a.duration = 300;
	w.open(a);
}


function copy(e){
	Ti.UI.Clipboard.clearText();
	if($.copyLink.getText())
	{
		Ti.UI.Clipboard.setText($.copyLink.getText()); 
		Ti.API.log('Clipboard.hasText(), should be true: ' + Ti.UI.Clipboard.hasText());
		var a = Titanium.UI.createAlertDialog({
			title:'',
			message:'Einladungs-Link erfolgreich kopiert. Teile ihn mit deinen Freunden auf Facebook, WhatsApp, Viber und wo '
		});
		/*a.addEventListener('click', function(e)
		{
			if (Ti.Platform.osname === 'android' && a.buttonNames === null) {
				alert('There was no button to click');
			} else {
				alert('You clicked ' + e.index);
			}
		});*/
		a.show();
	}
}

/*SMS and EMAIL methods */



function sendEmail(e) {
    //alert($.label.text);
    if(allowInvitation){
	    var emailDialog = Ti.UI.createEmailDialog();
		emailDialog.subject = "Mach jetzt auch mit!";
		emailDialog.toRecipients = [];
		emailDialog.messageBody = emailMsgBody; 
		//var f = Ti.Filesystem.getFile('cricket.wav');
		//emailDialog.addAttachment(f);
		emailDialog.open();
	}
}


function sendSms(e){
	
	if(allowInvitation)
	{
		if(Ti.Platform.osname === 'iphone'){
			var smsDialog = Alloy.Globals.smsModule.createSMSDialog({
		    recipients: [],
		    messageBody:smsMsgBody,
		    barColor: 'red'});
		
		    smsDialog.open({animated: true});
		}else{
			/*Android */
			var intent = Ti.Android.createIntent({
			    	action : Ti.Android.ACTION_VIEW,
			    	type : "vnd.android-dir/mms-sms"
				});
			 
			intent.putExtra("address","");
			intent.putExtra("sms_body",smsMsgBody);
			Ti.Android.currentActivity.startActivity(intent);
				
			Ti.Android.currentActivity.startActivityForResult(intent,function(e){
			    //if (e.resultCode === SMS_SENT) {
			    //    alert("SMS SENT");
			    //}
			});
		}
	}
}