/*=======Internet Connection Notification =======
 =======================================*/

function connectionNotification(online){
	Alloy.Globals.online = online;
	if(!online){
		$.ds.contentview.setTop("64dp");
		$.ds.internetNotification.setVisible(true);
	 }else{	 	
	 	$.ds.contentview.setTop("44dp");
	 	$.ds.internetNotification.setVisible(false);
	 }
};

connectionNotification(Titanium.Network.online);


Titanium.Network.addEventListener('change', function(e)
{
	connectionNotification(e.online);
	
});
/*=======Internet Connection Notification  END =======
 =======================================*/


var leftData = [];
var rightData = [];


// Pass data to widget leftTableView and rightTableView
Alloy.Globals.setupUI = function(d){
	var leftData = [];
	var rightData = [];
	
	
	if(d){
		var leftMenuData=[];
					 	  
		var rightMenuData=[{name:"HOME",viewsrc:"home",image:null,props:null,scroll:false},
						  {name:"SO GEHT’S",viewsrc:"howtoplay",image:null,props:null,scroll:false},
						  {name:"EINLADUNG",viewsrc:"invitation",image:null,props:null,scroll:false},
						  {name:"FAQ",viewsrc:"faq",image:null,props:null,scroll:false},
						  {name:"BEDINGUNGEN",viewsrc:"terms",image:null,props:null,scroll:false}
						  ];
	}else{
		var leftMenuData=[];
					  
   		var rightMenuData=[{name:"HOME",viewsrc:"home",image:null,props:null,scroll:false},
						  {name:"SO GEHT’S",viewsrc:"howtoplay",image:null,props:null,scroll:false},
						  {name:"FAQ",viewsrc:"faq",image:null,props:null,scroll:false},
						  {name:"BEDINGUNGEN",viewsrc:"terms",image:null,props:null,scroll:false}];
	}
	
	/*for(var i=0;i<leftMenuData.length;i++)
	{
		var rowl = Alloy.createController('leftMenuRow', leftMenuData[i]).getView();
		leftData.push(rowl);
	}*/
	
	for(var i=0;i<rightMenuData.length;i++)
	{
		var rowr = Alloy.createController('rightMenuRow', rightMenuData[i]).getView();
		rightData.push(rowr);
	}
	
	if (Ti.Platform.osname === 'iphone'){
		$.ds.leftTableView.separatorStyle = Ti.UI.iPhone.TableViewSeparatorStyle.NONE; //remove the separator lines
	}
	
	//remove children first 
	$.ds.leftTableView.removeAllChildren();
	$.ds.rightTableView.removeAllChildren(); 
	
	//add the rows
	$.ds.leftTableView.data = leftData;
	$.ds.rightTableView.data = rightData;
	
};

function initFirstView(){
	var firstView = Alloy.createController("home").getView();
		$.ds.contentview.addView(firstView);
		$.ds.contentview.setCurrentPage(0);
		$.ds.navTitle.setText("");
}

initFirstView();
if(Ti.App.Properties.hasProperty('user') || Ti.App.Properties.hasProperty('userMenu')  )
{
//TODO: if user is logged in then we need to pass false
	Alloy.Globals.setupUI(true);
}else{
	Alloy.Globals.setupUI(false);
}





Ti.App.addEventListener("changeView", function(e) {
	//setTimeout(function(){
		rowSelect(e);
		if (Ti.Platform.name != "iPhone OS"){
    		toggleloadingView(true);	
    	}
	//}, 100);
	
});



function rowSelect(e) {
	var views = $.ds.contentview.getViews();
	var currentView = views[$.ds.contentview.getCurrentPage()];
	
	Alloy.Globals.previousView = {row:{name:$.ds.navTitle.getText(),viewsrc:currentView.id,scroll:false}};
	if (currentView.id != e.row.viewsrc) {
		var num = _.map(views, function(num, key){ return num.id; }).indexOf(e.row.viewsrc);
		if(num==-1)
		{
			//Ti.API.info("new View");
			var newView = Alloy.createController(e.row.viewsrc).getView();
			$.ds.contentview.addView(newView);
			//Ti.API.info(JSON.stringify(newView));
			setTimeout(function(){
    			if(e.row.scroll){
					$.ds.contentview.scrollToView(newView);
				}else{
					$.ds.contentview.setCurrentPage(views.length);
				}
			}, 500);
		}else
		{
			//Ti.API.info("num");
			//alert(views[num].id);
			if(e.row.scroll){
				$.ds.contentview.scrollToView(views[num]);
			}else{
				$.ds.contentview.setCurrentPage(num);
			}
			
		}
		if(e.row.viewsrc=="home"){
			$.ds.navTitle.setText("");
		}else{
			$.ds.navTitle.setText(e.row.name);
		}
		Ti.App.fireEvent("view:"+e.row.viewsrc, e.row.props);
	}
	
}


function toggleloadingView(vis){
	if(vis)
	{
		$.ds.loadingView.setHeight(Titanium.UI.FILL);
		$.ds.loadingView.setVisible(true);
	}else{
		$.ds.loadingView.setHeight(0);
		$.ds.loadingView.setVisible(false);
	}
}

if (Ti.Platform.name != "iPhone OS"){
	$.ds.contentview.addEventListener("scrollend",function(e){
		toggleloadingView(false);
	});
}





// Swap views on menu item click
$.ds.leftTableView.addEventListener('click', function selectRow(e) {
	if(e.row.viewsrc!=""){
		rowSelect(e);
		if (Ti.Platform.name != "iPhone OS"){
			setTimeout(function(){
    			$.ds.toggleLeftSlider();
			}, 800);
		}else{
			$.ds.toggleLeftSlider();
		}
		
	}
});

if (Ti.Platform.name != "iPhone OS"){
	$.ds.contentview.addEventListener('scrollend',function(){
		toggleloadingView(false);
		
	});

}

$.ds.rightTableView.addEventListener('click', function selectRow(e) {
	if(e.row.viewsrc=="faq"){
		Titanium.Platform.openURL("https://pepsi.futbolnow.de/promo/web/faq");
	}else{
		if (Ti.Platform.name != "iPhone OS"){
				var views = $.ds.contentview.getViews();
				var currentView = views[$.ds.contentview.getCurrentPage()];
				if(currentView.id != e.row.viewsrc){
					rowSelect(e);
					toggleloadingView(true); // open loading screen 					
				}
				$.ds.toggleRightSlider();
				//setTimeout(function(){
	    		//	toggleloadingView(false);
				//}, 1000);
		}else{
			rowSelect(e);
			setTimeout(function(){
				$.ds.toggleRightSlider();
			}, 500);
		}
	}	
	
});

// Set row title highlight colour (left table view)
var storedRowTitle = null;
$.ds.leftTableView.addEventListener('touchstart', function(e) {
	//storedRowTitle = e.row.customTitle;
	//storedRowTitle.color = "#FFF";
});
$.ds.leftTableView.addEventListener('touchend', function(e) {
	//storedRowTitle.color = "#666";
});
$.ds.leftTableView.addEventListener('scroll', function(e) {
	//if (storedRowTitle != null)
	//	storedRowTitle.color = "#666";
});

// Set row title highlight colour (right table view)
var storedRowTitle = null;
$.ds.rightTableView.addEventListener('touchstart', function(e) {
	//storedRowTitle = e.row.customTitle;
	//storedRowTitle.color = "#FFF";
});
$.ds.rightTableView.addEventListener('touchend', function(e) {
	//storedRowTitle.color = "#666";
});
$.ds.rightTableView.addEventListener('scroll', function(e) {
	//if (storedRowTitle != null)
	//	storedRowTitle.color = "#666";
});

Ti.App.addEventListener("sliderToggled", function(e) {
	
	// This is inorder to hide the keyboard
	if(e.direction!="reset"){
		var views = $.ds.contentview.getViews();
		var currentView = views[$.ds.contentview.getCurrentPage()];
		if(currentView.id=="coupon" || currentView.id=="pinDialog" || currentView.id=="redeemForm" ){
			var scrollView = currentView.getChildren()[0];
			var childObjs =scrollView.getChildren();
			for (var i=0; i<childObjs.length;i++) {
	    		if(childObjs[i].id=="hideKeyboard"){
	    			childObjs[i].focus();
	    			childObjs[i].blur();
	    		}
			}
		}
	}
	
	if (e.direction == "right") {
		$.ds.leftMenu.zIndex = 2;
		$.ds.rightMenu.zIndex = 1;
	} else if (e.direction == "left") {
		$.ds.leftMenu.zIndex = 1;
		$.ds.rightMenu.zIndex = 2;
	}
});




var preloadViewsList = ["coupon",
						"winnerView",
						"nowinnerView",
						"pinDialog",
						"redeemForm",
						"reviewRedeemForm",
						"redeemSuccess",
						"howtoplay",
						"invitation",
						"terms"];
	
function preloadViews(){
	var views = $.ds.contentview.getViews();
	for(var i=0;i<preloadViewsList.length; i++){
		var num = _.map(views, function(num, key){ return num.id; }).indexOf(preloadViewsList[i]);
		if(num==-1){
			var newView = Alloy.createController(preloadViewsList[i]).getView();
			$.ds.contentview.addView(newView);
		}
	}
}


if (Ti.Platform.osname === 'iphone'){
	
	$.win.statusBarStyle = Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT;
	$.win.open({
		transition : Titanium.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
	});
}
else{
	$.win.setOrientationModes([Titanium.UI.PORTRAIT]); 
	$.win.open();
}

$.ds.logo.addEventListener("click",function(){
	Ti.App.fireEvent("changeView", 
			{row : {name:"Home",viewsrc:"home",props:null,scroll:false}}
    );
});


$.win.addEventListener("open",function(e){
	preloadViews();
});
