// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
if(Ti.Platform.osname === 'iphone'){
	Alloy.Globals.smsModule = require("com.omorandi");
}

Alloy.Globals.online =Titanium.Network.online;

Alloy.Globals.userMsisdn ="";
if(Ti.App.Properties.hasProperty('user')){
	Alloy.Globals.userMsisdn=Ti.App.Properties.getObject('user')['msisdn'];
};

//Alloy.Globals.serverUrl = "http://10.130.35.77/promo/web/"; //Promo Staging
//Alloy.Globals.serverUrl = "http://10.1.6.29:8080/mgc/cgi/api/web/"; //Kostas pc mgc
//Alloy.Globals.serverUrl = "http://10.1.3.39:8050/promo/web/"; //Kostas -George pc Promo
//Alloy.Globals.serverUrl = "http://10.1.2.43:8050/promo/web/"; //Sofia pc Promo
Alloy.Globals.serverUrl = "https://pepsi.futbolnow.de/promo/web/"; //Production





/*==========POST function===============*/
Alloy.Globals.doPost = function(data,url,loadCallBack,errCallBack){
	var xhr = Titanium.Network.createHTTPClient();
	//need to clear anyCookie saved
	//xhr.clearCookies(Alloy.Globals.hosturl);
	xhr.onload = loadCallBack;
	xhr.onerror = errCallBack;
	xhr.timeout = 10000;
	xhr.open("POST",url);
	
	//if(cookie){xhr.setRequestHeader("Cookie",cookie);}
	xhr.send(data);
};
/*==========POST function END===============*/

/*==========GET function===============*/
Alloy.Globals.doGET = function(url,loadCallBack,errCallBack){
	var xhr = Titanium.Network.createHTTPClient();
	//need to clear anyCookie saved
	//xhr.clearCookies(Alloy.Globals.hosturl);
	xhr.onload = loadCallBack;
	xhr.onerror = errCallBack;
	xhr.timeout = 10000;
	xhr.open("GET",url);
	 // in milliseconds
	//if(cookie){xhr.setRequestHeader("Cookie",cookie);}
	xhr.send();
};
/*==========GET function END===============*/


/*=======PUSH Notifications=======
 * 
 * 
 * 
 * */


Alloy.Globals.UrbanAirship = require('ti.urbanairship');
Titanium.Network.addEventListener("change",function(){
	if(Titanium.Network.online){
		Alloy.Globals.online = Titanium.Network.online;
		if(Ti.App.Properties.hasProperty('PushToken'))
		{
			startPushNotification();
		}
	}
});

function startPushNotification(){
	var UrbanAirship = Alloy.Globals.UrbanAirship;
	if(Alloy.Globals.online){
		Ti.API.info("======================1=====================");
		if (Ti.Platform.name == "iPhone OS"){
			Alloy.Globals.isiOS = true; 
			UrbanAirship.tags = [ 'pepsiFootball' ];
			UrbanAirship.alias = 'pepsiFootBall';
			UrbanAirship.autoBadge = true;
			UrbanAirship.autoResetBadge = true;
			
			Ti.Network.registerForPushNotifications({
			    types:[
			        Ti.Network.NOTIFICATION_TYPE_BADGE,
			        Ti.Network.NOTIFICATION_TYPE_ALERT,
			        Ti.Network.NOTIFICATION_TYPE_SOUND
			    ],
			    success: function(e) {
			        var token = e.deviceToken;
			        UrbanAirship.registerDevice(token);
					//alert("Your token is: "+token); 
					Ti.App.Properties.setString('PushToken', token);
					Alloy.Globals.PushToken = token;
					   
			    },
			    error: function(e) {
			        //alert("Error: " + e.error);
			    },
			    callback: function(e) {
			        UrbanAirship.handleNotification(e.data);
			        //alert(e.data);
			        var dialog = Ti.UI.createAlertDialog({
				    message: e.data.alert,
				    ok: 'OK',
				    title: 'Anzeige'
				  }).show();
			    }
			});
		}else{
			Alloy.Globals.isiOS = false; 
			Ti.API.info('test');
			UrbanAirship.tags = [ 'pepsiFootballAndroid' ];
			UrbanAirship.alias = 'pepsiFootBallAndroid';
			UrbanAirship.showOnAppClick = true;
			UrbanAirship.pushEnabled = false;
			UrbanAirship.pushEnabled = true;
			//Ti.API.info('Urban Push id =' + UrbanAirship.pushId);
			
			function eventCallback(e) {
				if (e.clicked) {
					//Ti.API.info('User clicked a notification');
				} else {
					//Ti.API.info('Push message received');
				}
				//Ti.API.info('  Message: ' + e.message);
				//Ti.API.info('  Payload: ' + e.payload);
				
				var dialog = Ti.UI.createAlertDialog({
				    message: e.message,
				    ok: 'OK',
				    title: 'Anzeige'
				  }).show();
			}
			
			function eventSuccess(e) {
				//Ti.API.info('Received device token: ' + e.deviceToken);
				//alert('Device token='+e.deviceToken);
				Ti.App.Properties.setString('PushToken', e.deviceToken);
				Alloy.Globals.PushToken = e.deviceToken;
			}
			
			function eventError(e) {
				Ti.API.info('Error:' + e.error);
				/*var alert = Ti.UI.createAlertDialog({
					title:'Error',
					message:e.error
				});
				alert.show();*/
				//alert("An error occured, Please check your Internet Connection");
			}
			
			UrbanAirship.addEventListener(UrbanAirship.EVENT_URBAN_AIRSHIP_CALLBACK, eventCallback);
			UrbanAirship.addEventListener(UrbanAirship.EVENT_URBAN_AIRSHIP_SUCCESS, eventSuccess);
			UrbanAirship.addEventListener(UrbanAirship.EVENT_URBAN_AIRSHIP_ERROR, eventError);
		}
	}
}

startPushNotification();
/*=======PUSH Notifications END=======
 =======================================*/


